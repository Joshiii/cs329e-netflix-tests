#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    #Test normal
    def test_eval_1(self):
        r = StringIO("9998:\n1288730\n2536567\n1107317\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "9998:\n3.2\n3.3\n3.6\n0.91\n")

    #Test if customer didn't rate movie
    def test_eval_2(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n1107317\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.2\n3.4\n3.8\nCustomer didn't rate this movie.\n0.93\n")
        
    #
    def test_eval_3(self):
        r = StringIO("5103:\n1464635\n2384400\n866343\n1250426\n1258013\n2137543\n1551962\n1167401")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "5103:\n4.2\n4.2\n4.4\n3.9\n4.1\n3.9\n4.1\n4.6\n0.80\n")
    
    #Test if movie doesn't have any ratings
    def test_eval_4(self):
        r = StringIO("9998:\n5103:\n1464635\n2384400\n866343\n1250426\n1258013\n2137543\n1551962\n1167401")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "9998:\n5103:\n4.2\n4.2\n4.4\n3.9\n4.1\n3.9\n4.1\n4.6\n0.80\n")
        

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
